#!/bin/sh

date=`date +"%Y-%m-%d"`

backup_if_exists () {
	if [ -e $1 ]; then
		echo "$1 -> $1.backup.$date"
		mv $1 $1.$date
	fi
}

backup_if_exists ~/.vim
backup_if_exists ~/.vimrc
backup_if_exists ~/.vimrc_local

pwd=`pwd`
ln -s $pwd/.vim ~/
ln -s $pwd/.vimrc ~/

echo "" > ~/.vimrc_local

