" pathogen
execute pathogen#infect()

" some stuff
filetype plugin indent off
filetype plugin on

" source local-specific settings (office/home/etc)
source ~/.vimrc_local

" set block cursor even in gui mode
set guicursor =

" tabs policy
set autoindent shiftwidth=0 tabstop=2 noexpandtab

" auto indent
set nocindent
set nosmartindent
set formatoptions+=cro

" line numbers
set nu

" highlight search
set hls

" icons for folders
let g:NERDTreeDirArrowExpandable  = '+'
let g:NERDTreeDirArrowCollapsible = '-'

" toggle tree navigation
nnoremap <C-T> :NERDTreeToggle<CR>

" enable nerdtree by default
autocmd VimEnter * NERDTree

" jump to file, not to tree
autocmd VimEnter * wincmd p

" splits navigation
nnoremap <C-J> <C-W><C-H>| " left
nnoremap <C-L> <C-W><C-L>| " right
nnoremap <C-I> <C-W><C-K>| " up
nnoremap <C-K> <C-W><C-J>| " down

" save
nnoremap <F2> :wa<CR>

" jump to definition/implementation
nmap <silent> <F3> <Plug>(coc-definition)
nmap <silent> <F4> <Plug>(coc-implementation)

" fuzzy search by filename
nnoremap <F7> :FZF<CR>

" try to close buffer
nnoremap <F8> :bp<bar>sp<bar>bn<bar>bd<CR>

" skip unsaved and quit
nnoremap <F10> :qa!<CR>

" save and quit
nnoremap <F12> :wqa<CR>

" navigate through buffers
nnoremap <S-Right> :bnext<CR>
nnoremap <S-Left>  :bprev<CR>

" jump to begin/end of file
nnoremap <C-Up> gg
nnoremap <C-Down> G

" delete line instead of dd
nnoremap <leader>d dd

" tune nerd commenter
let g:NERDCreateDefaultMappings = 0
let g:NERDDefaultAlign      = 'start'
let g:NERDCommentEmptyLines = 1

" toggle comment with \/
nnoremap <leader>/ :call NERDComment(0,"invert")<CR>
vnoremap <leader>/ :call NERDComment(0,"invert")<CR>

" rich colors palette for terminal
set termguicolors

" make vertical split more nice
hi VertSplit cterm = none

" add vertical separators to buffer explorer
let g:buftabline_separators = 1

" make status line nice
hi StatusLine   ctermbg=0 ctermfg=15
hi StatusLineNC ctermbg=0 ctermfg=7

" backspace fix :(
set backspace=indent,eol,start

" configure lightline color
let g:lightline = { 'colorscheme': 'one' }

" force syntax on
syntax on

" do not change vim pwd when open bookmark
let g:startify_change_to_dir = 0

" setup chromatica highlight for c++
let g:chromatica#enable_at_startup = 1
let g:chromatica#libclang_path     = '/opt/clang/lib/libclang.so'
let g:chromatica#use_pch           = 0

" enable gitgutter everywhere
set signcolumn=yes

" tune members highlight
hi Member       guifg=DarkCyan
hi EnumConstant guifg=SeaGreen

" coc completion
inoremap <silent><expr> <c-space> coc#refresh()

" close coc preview window after completion is done
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

" do not show preview for autocomplete
set completeopt-=preview

" use the silver searcher
let g:agprg="ag -i --vimgrep"
let g:ag_highlight=1

" map s to the ag command for quick searching
nnoremap s :Ag<SPACE>
